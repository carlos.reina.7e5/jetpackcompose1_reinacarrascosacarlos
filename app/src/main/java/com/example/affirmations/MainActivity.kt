/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.affirmations.model.Affirmation
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.viewmodel.MainViewModel

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      // TODO 5. Show screen
      AffirmationApp()
    }
  }
}

@Composable
fun AffirmationApp(mainViewModel: MainViewModel = viewModel()) {
  // TODO 4. Apply Theme and affirmation list
  val uiState by mainViewModel.uiState.collectAsState()

  AffirmationsTheme {
    Column {
      AffirmationList(affirmationList = uiState)
    }
  }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
  // TODO 3. Wrap affirmation card in a lazy column
  LazyColumn(modifier = modifier) {
    items(affirmationList) {
        affirmation -> AffirmationCard(
        affirmation = affirmation,
        modifier = Modifier.padding(all = 5.dp)
      )
    }
  }
}

@Composable
fun AffirmationCard(
  affirmation: Affirmation,
  modifier: Modifier = Modifier)
{
  // TODO 1. Your card UI
  var expanded by remember { mutableStateOf(false) }
  val color by animateColorAsState(
    targetValue = if (expanded) colorResource(id = R.color.purple_500) else MaterialTheme.colors.surface
  )
  Card(
    modifier = modifier, elevation = 5.dp
  ) {
    Column(
      modifier = Modifier
        .background(color = color)
        .animateContentSize(
          animationSpec = spring(
            dampingRatio = Spring.DampingRatioMediumBouncy,
            stiffness = Spring.StiffnessLow
          )
        )
    ) {
      Row(
        modifier = Modifier.fillMaxSize(), verticalAlignment = Alignment.CenterVertically
      ) {
        Image(
          painter = painterResource(id = affirmation.imageResourceId),
          contentDescription = stringResource(id = affirmation.stringResourceId),
          modifier = Modifier.padding(8.dp)
        )
        Text(
          text = stringResource(id = affirmation.stringResourceId),
          modifier = Modifier
            .weight(1f)
            .padding(8.dp),
          fontFamily = FontFamily.Monospace,
          fontSize = 15.sp
        )
        AffirmationButton(expanded = expanded, onClick = { expanded = !expanded })
      }
      if (expanded) {
        Description(stringResource(id = affirmation.descResourceId), id = affirmation.id)
      }
    }
  }
}

@Composable
fun AffirmationButton(
  expanded: Boolean,
  onClick: () -> Unit,
){
  IconButton(onClick = onClick) {
    Icon(
      imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
      tint = MaterialTheme.colors.secondary,
      contentDescription = "ItemButton"
    )
  }
}

@Composable
fun Description(description: String, id: Int) {
  Row () {
    Column(
      modifier = Modifier
        .padding(20.dp)
        .weight(5f)
    ) {
      Text(text = "Description: ", fontSize = 16.sp)
      Text(text = description, fontSize = 12.sp,
           overflow = TextOverflow.Ellipsis,
           maxLines = 2
      )
    }

    Spacer(modifier = Modifier.weight(0.5f))

    val mContext = LocalContext.current

    Button(
      onClick = { mContext.startActivity(Intent(mContext, DetailActivity::class.java).putExtra("affirmationId", id))},
      modifier = Modifier
        .padding(20.dp)
        .align(Alignment.CenterVertically),
      colors = ButtonDefaults.buttonColors(colorResource(id = R.color.teal_200))
    ) {
      Text(text = "See details", fontSize = 12.sp, fontWeight = FontWeight.ExtraBold)
    }
  }
}

@Preview
@Composable
private fun AffirmationCardPreview() {
  // TODO 2. Preview your card
  AffirmationCard(
    affirmation = Affirmation(
      1,
      R.string.affirmation3,
      R.drawable.image3,
      R.string.desc3
    )
  )
}
